let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.find({}, function (err, bicicletas) {
    res.status(200).json({
      bicicletas: bicicletas,
    });
  });
};

exports.bicicleta_create = function (req, res) {
  let bici = new Bicicleta(req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);
  console.log(bici);

  res.status(200).json({
    bicicleta: bici,
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
};

exports.bicicleta_update = function (req, res) {
  let {id, color, modelo, latitud, lng} = req.body;
  let bici = Bicicleta.findById(req.params.id);

  bici.id = id;
  bici.color = color;
  bici.modelo = modelo;
  bici.ubicacion = [latitud, lng];

  res.status(200).json({bicicleta: bici});
};
