let Usuario = require("../models/Usuario");
let Token = require("../models/Token");

module.exports = {
  confirmationGet: function (req, res, next) {
    Token.findOne({token: req.params.token}, function (err, token) {
      if (!token)
        return res
          .status(400)
          .send({type: "not-verified", msg: "No se encontró el token"});
      Usuario.findById(token.userId, function (err, usuario) {
        if (!usuario) return res.status(400).send({msg: "no se encontró el usuario asociado a ese token"});
        if (usuario.verificado) return res.redirect("/usuarios");
        usuario.verificado = true;
        usuario.save(function (err) {
          if (err) {
            return res.status(500).send({msg: err.message});
          }
          res.redirect("/");
        });
      });
    });
  },
};
