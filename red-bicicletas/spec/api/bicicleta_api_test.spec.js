let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");
let request = require("request");
let server = require("../../bin/www");

let baseUrl = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeEach(function (done) {
    let mongoDB = "mongodb+srv://admin:admin@cluster0.re0h4.mongodb.net/testdb";
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET BICICLETAS /", () => {
    it("Status 200", (done) => {
      request.get(baseUrl, function (err, response, body) {
        expect(response.statusCode).toBe(200);
        let result = JSON.parse(body);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("STATUS 200", (done) => {
      let headers = {'content-type': 'application/json'};
      let newBici = {"code": 10, color: "verde", "modelo": "urbana", "lat": 4, "lng": -74};
      request.post(
        {
          headers: headers,
          url: `${baseUrl}/create`,
          body: newBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          let bici = JSON.parse(body).bicicleta;
          console.log(bici);
          expect(bici.color).toBe("rojo");
          expect(bici.ubicacion[0]).toBe(4);
          expect(bici.ubicacion[1]).toBe(-74);
          done();
        }
      );
    });
  });
});
