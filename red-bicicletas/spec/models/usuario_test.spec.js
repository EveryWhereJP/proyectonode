let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");
let Usuario = require("../../models/Usuario");
let Reserva = require("../../models/Reserva");

describe("Testing usuarios", function () {
  beforeEach(function (done) {
    let mongoDB = "mongodb+srv://admin:admin@cluster0.re0h4.mongodb.net/testdb";
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are conected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe("Cuando un usuario reserva una bici", () => {
    it("debe existir la reserva", (done) => {
      const usuario = new Usuario({nombre: "Jhosua"});
      usuario.save();
      const bicicleta = new Bicicleta({code: 1, color: "rojo", modelo: "urbano"});
      bicicleta.save();

      let hoy = new Date();
      let mañana = new Date();
      mañana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, mañana, function (err, reserva) {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec(function (err,reservas) {
            console.log(reservas[0]);
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
