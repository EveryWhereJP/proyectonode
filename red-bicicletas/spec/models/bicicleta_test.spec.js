let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");

describe("Testing Bicicletas", function () {
  beforeEach(function (done) {
    let mongoDB = "mongodb+srv://admin:admin@cluster0.re0h4.mongodb.net/testdb";
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "Conecction error"));
    db.once("open", function () {
      console.log("We are conected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("crea una instancia de Bicicleta", () => {
      let bici = Bicicleta.createInstance(1, "rojo", "urbana", [4.56, -74.45]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("rojo");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(4.56);
      expect(bici.ubicacion[1]).toEqual(-74.45);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("comienza vacio", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("agregar solo una bici", (done) => {
      let bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
      Bicicleta.add(bici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(bici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("debe devolver la bici con el code 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        let bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
        Bicicleta.add(bici, function (err, newBici) {
          if (err) console.log(err);

          let bici2 = new Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
          Bicicleta.add(bici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(1, function (err, targetBici) {
              expect(targetBici.code).toBe(bici.code);
              expect(targetBici.color).toBe(bici.color);
              expect(targetBici.modelo).toBe(bici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe("Bicicleta.removeByCode", () => {
    it("elimina la bici con el code 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        let bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
        Bicicleta.add(bici, function (err, newBici) {
          if (err) console.log(err);
          Bicicleta.removeByCode(1, function (err, targetBici) {
            expect(bicis.length).toBe(0);
            done();
          });
        });
      });
    });
  });
});

/* beforeEach(() => {
  Bicicleta.allBicis = [];
});

describe("Bicicleta.allBicis", () => {
  it("Comienza vacío", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe("Bicicleta.add", () => {
  it("agregamos una ", () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    let bici = new Bicicleta(1, "morado", "montaña"[(4.4984, -7492828)]);
    Bicicleta.add(bici);
    expect(Bicicleta.allBicis[0]).toBe(bici);
  });
});

describe("Bicicleta.findById", () => {
  it("Debe devolver la bici con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    let bici1 = new Bicicleta(1, "verde", "urbana", [4.7272, -748282]);
    let bici2 = new Bicicleta(2, "cafe", "montaña", [4.7232, -748582]);
    Bicicleta.add(bici1);
    Bicicleta.add(bici2);
    let targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(bici1.color);
    expect(targetBici.modelo).toBe(bici1.modelo);
  });
});

describe("Bicicleta.removeById", () => {
  it("Eliminar la bici con el id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    let bici1 = new Bicicleta(1, "verde", "urbana", [4.7272, -748282]);
    let bici2 = new Bicicleta(2, "cafe", "montaña", [4.7232, -748582]);
    Bicicleta.add(bici1);
    Bicicleta.add(bici2);

    Bicicleta.removeById(1);

    expect(Bicicleta.allBicis.length).toBe(1);
  });
});
 */
