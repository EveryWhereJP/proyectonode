const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const passport = require("./config/passport");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const jwt = require("jsonwebtoken");
require("dotenv").config();

const indexRouter = require("./routes/index");
const usuarioRouter = require("./routes/usuarios");
const bicicletaRouter = require("./routes/bicicletas");
const tokenRouter = require("./routes/token");
const bicicletaRouterAPI = require("./routes/api/bicicletas");
const usuarioRouterAPI = require("./routes/api/usuarios");
const authRouterAPI = require("./routes/api/auth");

let store;
if(process.env.NODE_ENV ==="development"){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection:'sessions',
  });
  store.on("error",function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}
const app = express();

app.set("secretKey", "jwt_pwd_!2u3u2h2");

app.use(
  session({
    cookie: {maxAge: 240 * 60 * 60 * 1000},
    store: store,
    saveUninitialized: true,
    resave: true,
    secret: "red_bicis.232o32ion342k32m32ih3u3bg2b32ehje3yug32823u329023yhbe3",
  })
);

const mongoose = require("mongoose");
const Usuario = require("./models/Usuario");
const Token = require("./models/Token");
const { assert } = require("console");

/* Si estoy en ambiente de desarrollo */
/* const mongoDB ="mongodb://localhost/red_biciceltas" */
/* si no */
let mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB conection error"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get("/login", function (req, res) {
  res.render("session/login");
});

app.post("/login", function (req, res, next) {
  /* passport */
  passport.authenticate("local", function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render("session/login", {info});
    req.login(usuario, function (err) {
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  req.logOut();
  res.redirect("/");
});

app.get("/forgotPassword", function (req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function (req, res) {
  Usuario.findOne({email: req.body.email}, function (err, usuario) {
    if (!usuario)
      return res.render("session/forgotPassword", {
        info: {message: "No existe el email para un usuario existente"},
      });
    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log("session/forgotPasswordMessage");
    });
    res.render("session/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", function (req, res, next) {
  Token.findOne({token: req.params.token}, function (err, token) {
    if (!token)
      return res.status(400).send({
        type: "not-verified",
        msg:
          "No existe un usuario asociado al token, verifique que su token no haya expirado",
      });

    Usuario.findById(token.userId, function (err, usuario) {
      if (!usuario)
        return res.status(400).send({msg: "No existe un usuario asociado al token"});
      res.render("session/resetPassword", {errors: {}, usuario: usuario});
    });
  });
});

app.post("/resetPassword", function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {confirm_password: {message: "No coincide con el password ingresado"}},
      usuario: new Usuario({email: req.body.email}),
    });
    return;
  }

  Usuario.findOne({email: req.body.email}, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render("session/resetPassword", {
          errors: err.erros,
          usuario: new Usuario({email: req.body.email}),
        });
      } else {
        res.redirect("/login");
      }
    });
  });
});

app.use("/", indexRouter);
app.use("/usuarios", usuarioRouter);
app.use("/token", tokenRouter);
app.use("/bicicletas", loggedIn, bicicletaRouter);

app.use("/api/bicicletas", validarUsuario, bicicletaRouterAPI);
app.use("/api/usuarios", usuarioRouterAPI);
app.use("/api/auth", authRouterAPI);

app.use("/privacy_policy", function (req, res) {
  res.sendFile(path.join(__dirname,"public","privacy_policy.html"));
});

app.use("/google90ac20772c7e96cb", function (req, res) {
  res.sendFile(path.join(__dirname,"public","google90ac20772c7e96cb.html"));
});

app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/plus.login',
      , 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
));

app.get( '/auth/google/callback', 
    passport.authenticate( 'google', { 
        successRedirect: '/auth/google/success',
        failureRedirect: '/auth/google/failure'
}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("El usuario no esta logueado");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretKey"), function (
    err,
    decoded
  ) {
    if (err) {
      res.json({status: "error", message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log("jwt verify" + decoded);
      next();
    }
  });
}

module.exports = app;
