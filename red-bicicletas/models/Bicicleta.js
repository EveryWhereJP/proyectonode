let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {type: "2dsphere", sparse: true},
  },
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
  return new this({
    code,
    color,
    modelo,
    ubicacion,
  });
};

bicicletaSchema.methods.toString = function () {
  return `code: ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb);
};

bicicletaSchema.statics.add = function (bici, cb) {
  this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function (biciCode, cb) {
  return this.findOne({code: biciCode}, cb);
};

bicicletaSchema.statics.removeByCode = function (biciCode, cb) {
  return this.deleteOne({code: biciCode}, cb);
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);
