let map = L.map("main_map").setView([4.5976, -74.0461], 13);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="htpps://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

L.marker([4.64596, -74.08483]).addTo(map);
L.marker([4.645, -74.06]).addTo(map);
L.marker([4.6151, -74.0854]).addTo(map);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function (result) {
    console.log(result);
    result.bicicletas.forEach(function (bici) {
      L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
    });
  },
});
