let express = require('express');
let router =express.Router();

let bicicletaControllerApi=require('../../controllers/api/bicicletaControllerAPI');

router.get('/',bicicletaControllerApi.bicicleta_list);
router.post('/create',bicicletaControllerApi.bicicleta_create);
router.delete('/delete',bicicletaControllerApi.bicicleta_delete);
router.put('/:id/update',bicicletaControllerApi.bicicleta_update);

module.exports=router;