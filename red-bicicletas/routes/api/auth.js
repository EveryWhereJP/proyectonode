const express = require('express');
const router = express.Router();
const authApiController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate',authApiController.authenticate);
router.post('/forgotPassWord',authApiController.forgotPassWord);

module.exports= router;
